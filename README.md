[![License](https://img.shields.io/badge/license-Apache-brightgreen.svg)](https://www.apache.org/licenses/LICENSE-2.0)

Welcome to Split Dashboard UI tool
==================================

*Building a split dashboard? Don’t start from scratch. Grab the split dashboard template and admire your data in minutes.*

**The Split dashboard includes modes such as:**
* [x]  [Split dashboard view](https://gitlab.com/helen.aramyan/split-dashboard#view-mode)
* [x]  [Split Details and Difference](https://gitlab.com/helen.aramyan/split-dashboard#details-mode)

Table of Contents
=================

- [Installation steps](https://gitlab.com/helen.aramyan/split-dashboard#installation-steps)
- [Contribution](https://gitlab.com/helen.aramyan/split-dashboard#contribution)

## View mode
![split-dashboard](./split-dashboard.png "Split Dashboard")

## Details mode
![split-details](./split-details.png "Split Details and Difference")

## Installation steps

### In order to setup the dashboard, the following environment variables should be provided from the server side:
| **Environment Name** | **Description** |
| ------ | ------ |
| `ADMIN_KEY` | Admin key of the splits |
| `WORKSPACE_ID` | Splits workspace id | 
| `ENV_ID_1` | "Preview" splits environment id | 
| `ENV_ID_2` | "Prod" splits environment id | 

## Contribution

Big features are welcome but if you want to see your contributions included in Split Dashboard, we strongly recommend to contact us (see AUTHORS file to get contacts).
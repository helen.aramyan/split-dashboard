import React from 'react';
import { render } from 'react-dom';
import App from './components/App'
import './css/App.css'
import { ThemeProvider } from "@material-ui/core/styles";
import { createMuiTheme } from "@material-ui/core/styles";
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducers from './reducers/reducers'

const store = createStore(reducers, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

const Theme = createMuiTheme({
  palette: {
    primary: {
      main: "#FFFFFF"
    }
  }
})

render(
  <Provider store={store}>
    <ThemeProvider theme={Theme}>
      <App />
    </ThemeProvider>
  </Provider>,
  document.getElementById('splits')
);
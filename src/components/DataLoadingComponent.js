import React from 'react';
import { WaveLoading } from 'react-loadingg';

const DataLoadingComponent = () => {
    return (
        <WaveLoading color='#6c757d' style={{ align: 'right' }} />
    )
}

export default DataLoadingComponent;
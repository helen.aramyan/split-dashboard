import Button from '@material-ui/core/Button';
import { createMuiTheme, makeStyles, ThemeProvider } from '@material-ui/core/styles';
import React from 'react';

const useStyles = makeStyles((theme) => ({
    margin: {
        margin: theme.spacing(0.4)
    }
}));

const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#3FB8F0'
        },
        secondary: {
            main: '#58AC5E'
        }
    },
    typography: {
        button: {
            textTransform: 'none'
        }
    }
});

const buttonsStyle = {
    paddingTop: '12px'
}

const defaultStylePerButton = {
    cursor: 'default'
}

export default function CustomizedButtons() {
    const classes = useStyles();

    return (
            <div style={buttonsStyle}>

                <ThemeProvider theme={theme}>
                    <Button variant="outlined" color="primary" className={classes.margin} size='small' style={defaultStylePerButton}>
                        Special
        </Button>
                    <Button variant="outlined" color="secondary" className={classes.margin} size='small' style={defaultStylePerButton}>
                        On
        </Button>
                    <Button variant="outlined" color="inherit" className={classes.margin} size='small' style={defaultStylePerButton}>
                        Off
        </Button>
                </ThemeProvider>
            </div>
    );
}

import React from 'react';
import Toolbar from '@material-ui/core/Toolbar';
import { Container, Typography, Grid } from '@material-ui/core';
import ToolBarButtons from './ToolBarButtons';
import SearchComponent from './SearchComponent';

const toolBarStyles = {
  borderRight: '8px solid black',
  borderBottom: '8px solid black',
  borderColor: '#222c33',
  backgroundColor: '#323E45',
  color: '#FFFFFF',
  fontSize: 16,
  minHeight: '65px'
};

const iconStyle = {
  paddingTop: '17px',
  paddingBottom: '14px',
  maxWidth: '34px',
  minWidth: '34px'
}

const titleStyle = {
  paddingTop: '20px',
  paddingBottom: '20px',
  paddingLeft: '24px',
  fontFamily: 'Proxima Nova'
}

export default function ToolBar() {
  return (
    <>
      <Container>
        <Toolbar style={toolBarStyles}>
          <img src="WF_icon.png" alt="logo" style={iconStyle} />
          <Grid
            justify="space-between"
            container
            spacing={3}
          >
            <Grid item xs={5}>
              <Typography type='title' style={titleStyle}>
                SPLIT CONFIGURATION DASHBOARD
              </Typography>
            </Grid>
            <Grid item>
              <Grid
                justify="space-between"
                container
                spacing={1}
              >
                <Grid item xs={3}>
                  <SearchComponent />
                </Grid>
                <Grid item>
                  <ToolBarButtons />
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Toolbar>
      </Container >
    </>
  );
}

import TextField from '@material-ui/core/TextField';
import React, { Component } from "react";
import IconButton from "@material-ui/core/IconButton";
import InputAdornment from "@material-ui/core/InputAdornment";
import SearchIcon from "@material-ui/icons/Search";
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

const styles = {
    cssLabel: {
        color: '#475862',
        fontFamily: 'Proxima Nova',
        fontSize: 16
    },
    input: {
        color: "white",
        height: 28,
        width: 210
    }
};

class SearchComponent extends Component {
    _handleTextFieldChange = (e) => {
        this.props.onAddFilter(e.target.value);
    }

    render() {
        const { classes } = this.props;
        return (
            <TextField
                onChange={this._handleTextFieldChange}
                label="Enter a split name"
                color="primary"
                InputLabelProps={{
                    classes: {
                        root: classes.cssLabel
                    }
                }}
                InputProps={{
                    endAdornment: (
                        <InputAdornment>
                            <IconButton style={{ paddingRight: '0px', color: "white" }}>
                                <SearchIcon />
                            </IconButton>
                        </InputAdornment>
                    ),
                    className: classes.input
                }}
            />
        )
    }
}

SearchComponent.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default connect(
    state => ({
        mainState: state
    }),
    dispatch => ({
        onAddFilter: (filteredValue) => {
            dispatch({ type: 'FILTER', payload: filteredValue })
        }
    })
)(withStyles(styles)(SearchComponent));
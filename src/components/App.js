import React, { Component } from 'react';
import '../css/App.css';
import SplitDashboard from './SplitDashboard';
import ToolBar from './ToolBar';
import '../css/Font.css';
import { connect } from 'react-redux';

class App extends Component {
  render() {
    return (
      <div id='App'>
        <header>
          <ToolBar />
        </header>
        <SplitDashboard filteredValue={this.props.mainState.filter.filteredValue}/>
      </div>
    );
  }
}

export default connect(
  state => ({
    mainState: state
  }),
  dispatch => ({})
)(App)

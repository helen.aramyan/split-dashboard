import React from 'react';
import { DisappearedLoading } from 'react-loadingg';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

const LoadingComponent = () => {
    return (
        <TableRow style={{ border: 'solid #222c33', background: '#222c33', borderRight: '10px solid #222c33' }}>
            <TableCell>
                <DisappearedLoading color='#6c757d' style={{ align: 'center' }} />
            </TableCell>
            <TableCell>
                <DisappearedLoading color='#6c757d' style={{ align: 'center' }} />
            </TableCell>
            <TableCell>
                <DisappearedLoading color='#6c757d' style={{ align: 'center' }} />
            </TableCell>
        </TableRow >
    )
}

export default LoadingComponent;
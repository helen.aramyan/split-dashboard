import React, { PureComponent } from 'react';
import TableCell from '@material-ui/core/TableCell';
import DataLoadingComponent from './DataLoadingComponent';
import ReactDiffViewer from 'react-diff-viewer';
import SplitCopyButton from './SplitCopyButton';

const tableDetailCellStyle = {
    borderColor: '#222c33',
    fontSize: 16,
    color: '#FFFFFF',
    minHeight: 40,
    fontFamily: 'Proxima Nova',
    backgroundColor: '#323E45',
    borderRadius: 10,
    width: '100%',
    paddingTop: '0px', 
    paddingBottom: '0px'
}

const tableLoadingCellStyle = {
    borderColor: '#222c33',
    fontSize: 16,
    color: '#FFFFFF',
    minHeight: 40,
    fontFamily: 'Proxima Nova',
    backgroundColor: '#323E45',
    borderRadius: 10,
    width: '100%'
}

class SplitDetails extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            split_name: props.split_name,
            data: props.data
        }
    }

    async componentDidMount() {
        var requestToPreview = new Request(`./getSplit.php?SplitName=${this.state.split_name}&ENV=ENV1`);
        var requestToProd = new Request(`./getSplit.php?SplitName=${this.state.split_name}&ENV=ENV2`);
        Promise.all([
            await fetch(requestToPreview).then(data => data.json())
                .then(previewData => this.setState({ previewData })),
            await fetch(requestToProd).then(data => data.json())
                .then(prodData => this.setState({ prodData, loading: false }))
        ]
        )
    }

    render() {
        return (
            <>
                {this.state.loading ?
                    <>
                        <TableCell style={tableLoadingCellStyle}>
                            <DataLoadingComponent />
                        </TableCell>
                        <TableCell style={tableLoadingCellStyle} colSpan={2}/>
                    </>
                    :
                    <>
                        {!(this.state.previewData && this.state.prodData) ?
                            <>
                                <TableCell style={tableLoadingCellStyle}>
                                    <DataLoadingComponent />
                                </TableCell>
                                <TableCell style={tableLoadingCellStyle} colSpan={2}/>
                            </>
                            :
                                <>
                                    <TableCell colSpan={3} style={tableDetailCellStyle}>
                                    <SplitCopyButton previewData={JSON.stringify(this.state.previewData)} prodData={JSON.stringify(this.state.prodData)}/>
                                    <pre style={{ overflow: 'auto', maxWidth: '100%', maxHeight: '500px'}}>
                                    <ReactDiffViewer oldValue={JSON.stringify(this.state.previewData, 'null', '\t')} newValue={JSON.stringify(this.state.prodData, 'null', '\t')} splitView={true} showDiffOnly={false} useDarkTheme={true} leftTitle="Preview Split Info" rightTitle="Production Split Info"/>
                                    </pre>
                                    </TableCell>
                                </>
                        }
                    </>
                }
            </>
        );
    }
}

export default SplitDetails;
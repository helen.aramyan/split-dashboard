import React, { useState } from 'react';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import SplitDetails from "./SplitDetails";

const styles = () => ({
  tableRow: {
    "&$selected, &$selected:hover": {
      backgroundColor: "#475862"
    },
    "&:hover": {
      backgroundColor: "#475862",
      cursor: "pointer"
    }
  },
  selected: {cursor: 'pointer'},
  paper: {
    backgroundColor: '#323E45',
    padding: '1px',
  }
});

function getCorrectTreatmentValue(treatment) {
  if (typeof treatment !== 'string') return '';
  return treatment.charAt(0).toUpperCase() + treatment.slice(1).toLowerCase();
}

function getTreatmentCellValues(splitInfo, tableCellStyle) {
  let dataList = [splitInfo.previewDataTreatment, splitInfo.prodDataTreatment];
  let dataBody = [];
  // eslint-disable-next-line
  Object.values(dataList).map((dataCell, index) => {
    let tableStyleCopy = Object.assign({}, tableCellStyle);
    if (dataCell && dataCell.toLocaleLowerCase() === 'on') {
      tableStyleCopy.backgroundColor = '#58AC5E';
    } else if (dataCell && dataCell.toLocaleLowerCase() !== 'off') {
      tableStyleCopy.backgroundColor = '#3FB8F0';
    }
    if (index === 1) {
      tableStyleCopy.borderRight = ""
    }
    dataBody[index] = <TableCell style={tableStyleCopy} key={`${splitInfo.name}_treatment${index}`}>{getCorrectTreatmentValue(dataCell)}</TableCell>
  })

  return dataBody;
}

function SplitInfo(props) {
  const tableCellStyle = {
    borderRight: '8px solid black',
    borderColor: '#222c33',
    fontSize: 16,
    color: '#FFFFFF',
    paddingLeft: '24px',
    paddingTop: '10px',
    paddingBottom: '10px',
    maxHeight: 40,
    fontFamily: 'Proxima Nova'
  }

  const { allDatas } = props;
  const { classes } = props;
  const [selectedID, setSelectedID] = useState(null);

  return (Object.values(allDatas).map(splitInfo => {
    let dataBody = getTreatmentCellValues(splitInfo, tableCellStyle);

    return (
      <React.Fragment key={`${splitInfo.name}_data`}>
        <TableRow
          key={`row_${splitInfo.name}`}
          onClick={() => { setSelectedID(selectedID !== splitInfo.name ? splitInfo.name : null); }}
          selected={selectedID === splitInfo.name}
          classes={{ selected: classes.selected }}
          className={classes.tableRow}
          id={`${splitInfo.name}_row`}
        >
          <TableCell key={`${splitInfo.name}_name_cell`} style={tableCellStyle}>{splitInfo.name}</TableCell>
          {dataBody}
        </TableRow>
        {selectedID === splitInfo.name &&
          <TableRow
            key={'row_detail' + splitInfo.name}
            id={`split-details_${splitInfo.name}`}
          >
            <SplitDetails key={`split_details_data_${splitInfo.name}`} split_name={splitInfo.name} />
          </TableRow>
        }
      </React.Fragment>
    )
  })
  )
}

SplitInfo.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(SplitInfo);
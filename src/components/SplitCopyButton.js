import React, { useState } from 'react';
import { createMuiTheme, makeStyles, ThemeProvider } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import copy from 'copy-to-clipboard';
import Tooltip from '@material-ui/core/Tooltip';

const useStyles = makeStyles((theme) => ({
    margin: {
        margin: theme.spacing(0.4),
    }
}));

const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#3FB8F0'
        },
        secondary: {
            main: '#58AC5E'
        }
    },
    typography: {
        button: {
            textTransform: 'none'
        }
    }
});

const buttonsStyle = {
    paddingTop: '12px'
}

export default function SplitCopyButton(props) {
    const { previewData } = props;
    const { prodData } = props;
    const classes = useStyles();
    const [copySuccessEnv1, setCopySuccessEnv1] = useState('');
    const [copySuccessEnv2, setCopySuccessEnv2] = useState('');

    const copyToClipboard = (text, envName) => {
        copy(text);
        if(envName === 'ENV1'){
            setCopySuccessEnv1('Copied!');
            setCopySuccessEnv2(null);
        } else {
            setCopySuccessEnv2('Copied!');
            setCopySuccessEnv1(null);
        }
    };

    return (
        <div style={buttonsStyle}>
            <ThemeProvider theme={theme}>
                <Tooltip title={copySuccessEnv1? copySuccessEnv1: 'Copy'} placement="top-start">
                <Button variant="outlined" color="primary" className={classes.margin} size='small' onClick={() => copyToClipboard(previewData, 'ENV1')}>
                    <FileCopyIcon />
                    <div>Copy Split Json of Preview</div>
                </Button>
                </Tooltip>
                <Tooltip title={copySuccessEnv2? copySuccessEnv2: 'Copy'} placement="top-start">
                <Button variant="outlined" color="secondary" className={classes.margin} size='small' onClick={() => copyToClipboard(prodData, 'ENV2')}>
                    <FileCopyIcon />
                    <div>Copy Split Json of Production</div>
                </Button>
                </Tooltip>
            </ThemeProvider>
        </div>
    );
}

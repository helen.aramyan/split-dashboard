import React, { Component } from 'react';
import SplitInfo from './SplitInfo';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import Container from '@material-ui/core/Container';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import LoadingComponent from './LoadingComponent';

class SplitDashboard extends Component {
    state = {
        loading: true
    }

    headerStyles = {
        borderRight: '8px solid black',
        borderColor: '#222c33',
        minHeight: '60px',
        color: '#FFFFFF',
        background: '#323E45',
        fontSize: 16,
        paddingLeft: '24px',
        fontFamily: 'Proxima Nova'
    }

    headerStylesForProduction = {
        borderColor: '#222c33',
        minHeight: '60px',
        color: '#FFFFFF',
        background: '#323E45',
        fontSize: 16,
        paddingLeft: '24px',
        fontFamily: 'Proxima Nova'
    }

    async componentDidMount() {
        if (!this.state.allData) {
            var requestToPreview = new Request('./getEnvironmentSplits.php?ENV=ENV1');
            var requestToProd = new Request('./getEnvironmentSplits.php?ENV=ENV2');
            Promise.all([
                await fetch(requestToPreview).then(data => data.json())
                    .then(previewData => this.setState({ previewData })),
                await fetch(requestToProd).then(data => data.json())
                    .then(prodData => this.setState({ prodData, loading: false }))
            ]
            )
            this.setAllData(this.state.previewData, this.state.prodData);
            this.setState({filteredData: this.state.allData})
        }
    }

    getFilteredData() {
        const filteredValue = this.props.filteredValue;
        const allData = this.state.allData;
        const filteredData = this.state.filteredData;
        if (allData && filteredData) {
            let newFilteredData = Object.assign({}, allData);
            if(filteredValue) {
                Object.keys(newFilteredData)
                    .filter(split_name => !split_name.includes(filteredValue))
                    .forEach(key => delete newFilteredData[key])
            }   
            // eslint-disable-next-line
            this.state.filteredData = Object.assign({}, newFilteredData);
        }
        return this.state.filteredData;
    }

    setAllData(previewData, prodData) {
        let allData = {};
        // eslint-disable-next-line
        prodData.map(split => {
            allData[split.name] = ({
                name: split.name,
                prodDataTreatment: split.defaultTreatment
            })
        })
        // eslint-disable-next-line
        previewData.map(split => {
            allData[split.name] ?
                allData[split.name].previewDataTreatment = split.defaultTreatment
                :
                allData[split.name] = ({
                    name: split.name,
                    previewDataTreatment: split.defaultTreatment
                })
        }
        )

        this.setState({ allData });
    }


    render() {
        return (
            <>
                <Container>
                    <Paper style={{ background: '#323E45', borderRight: '8px solid black', borderColor: '#222c33' }}>
                        <Table style={{ tableLayout: "fixed" }}>
                            <TableHead>
                                <TableRow>
                                    <TableCell key="split_dashboard_title_column" style={this.headerStyles}>SPLIT NAME</TableCell>
                                    <TableCell key="split_dashboard_preview_column " style={this.headerStyles}>PREVIEW</TableCell>
                                    <TableCell key="split_dashboard_production_column" style={this.headerStylesForProduction}>PRODUCTION</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody key="split_dashboard_body">
                                <>
                                    {this.state.loading ?
                                        <LoadingComponent key="loading_icons_1" />
                                        :
                                        !this.getFilteredData() ?
                                            <LoadingComponent key="loading_icons_2" />
                                            : <SplitInfo key="split_dashboard" allDatas={this.state.filteredData} />    
                                    }
                                </>
                            </TableBody>
                        </Table>
                    </Paper>
                </Container>
            </>
        )
    }
}

export default SplitDashboard;
export default function filterReducer(state = [], action) {
    if (action.type === 'FILTER') {
        return {
            ...state,
            filteredValue: action.payload
        }
    }
    return state;
}
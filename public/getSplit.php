<?php

$WORKSPACE_ID = getenv("WORKSPACE_ID");
$ADMIN_KEY =getenv("ADMIN_KEY");
$split_name = urlencode($_GET['SplitName']);
$split_env = urlencode($_GET['ENV']);

$split_env = getEnvironmentId($split_env);
$request_url_split_per_environment = "https://api.split.io/internal/api/v2/splits/ws/$WORKSPACE_ID/$split_name/environments/$split_env";
$request_url_split_info = "https://api.split.io/internal/api/v2/splits/ws/$WORKSPACE_ID/$split_name";

function getResponse($request_url, $ADMIN_KEY)
{
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt_array($curl, array(
        CURLOPT_URL => $request_url,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Authorization: Bearer $ADMIN_KEY"
        ),
    ));
    $response = curl_exec($curl);
    curl_close($curl);

    return $response;
}

function getEnvironmentId($env_name)
{
    $environmentId = '';
    switch ($env_name) {
        case 'ENV1':
            $environmentId = getenv("ENV_ID_1");
            break;
        case 'ENV2':
            $environmentId = getenv("ENV_ID_2");
            break;
    }

    return $environmentId;
}

// Getting initial data for splits
$split_initial_data = getResponse($request_url_split_info, $ADMIN_KEY);
$split_initial_data = json_decode($split_initial_data);
// Getting performing second request for getting additional split fields
$split_additional_data = getResponse($request_url_split_per_environment, $ADMIN_KEY);
$split_additional_data = json_decode($split_additional_data);

// Collecting fields for split final data
$split_final_info->name = $split_initial_data->name;
$split_final_info->description = $split_initial_data->description;
$split_final_info->tags = $split_initial_data->tags;
$split_final_info->killed = $split_additional_data->killed;
$split_final_info->treatments = $split_additional_data->treatments;
$split_final_info->defaultTreatment = $split_additional_data->defaultTreatment;
$split_final_info->baselineTreatment = $split_additional_data->baselineTreatment;
$split_final_info->trafficAllocation = $split_additional_data->trafficAllocation;
$split_final_info->rules = $split_additional_data->rules;
$split_final_info->defaultRule = $split_additional_data->defaultRule;

header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
echo json_encode($split_final_info);


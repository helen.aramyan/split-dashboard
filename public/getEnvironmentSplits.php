<?php

$curl = curl_init();
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

$split_env = urlencode($_GET['ENV']);
$WORKSPACE_ID = getenv("WORKSPACE_ID");
$ADMIN_KEY =getenv("ADMIN_KEY");
$modified_json;

function getEnvironmentId($env_name)
{
    $environmentId = '';
    switch ($env_name) {
        case 'ENV1':
            $environmentId = getenv("ENV_ID_1");
            break;
        case 'ENV2':
            $environmentId = getenv("ENV_ID_2");
            break;
    }

    return $environmentId;
}

$split_env = getEnvironmentId($split_env);

curl_setopt_array($curl, array(
    CURLOPT_URL => "https://api.split.io/internal/api/v2/splits/ws/$WORKSPACE_ID/environments/$split_env?limit=50",
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "Authorization: Bearer $ADMIN_KEY"
    ),
));

$response = curl_exec($curl);
curl_close($curl);

$resources = json_decode($response);

$splits_array = array();
$array_with_all_split_data = $resources->objects;

foreach ($array_with_all_split_data as $key => $value) {
    $current_value = $array_with_all_split_data[$key];
    array_push($splits_array, array('name' => $current_value->name,
                                    'defaultTreatment' => $current_value->defaultRule[0] -> treatment,
                                    'treatments' => array($current_value -> treatments[0] -> name, $current_value -> treatments[1] -> name)));
}
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
echo json_encode($splits_array);
